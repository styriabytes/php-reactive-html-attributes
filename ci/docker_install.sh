#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it)
apt-get update -yqq
apt-get install git -yqq

# Install phpunit
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit-9.5.9.phar
chmod +x /usr/local/bin/phpunit

curl --location --output /usr/local/bin/phpunit72 https://phar.phpunit.de/phpunit-8.5.23.phar
chmod +x /usr/local/bin/phpunit72

# Here you can install any other extension that you need
# docker-php-ext-install pdo_mysql
