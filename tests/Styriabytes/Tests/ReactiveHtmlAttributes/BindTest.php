<?php

namespace Styriabytes\Tests\ReactiveHtmlAttributes;

use Styriabytes\ReactiveHtmlAttributes\Bind;
use PHPUnit\Framework\TestCase;

final class BindTest extends TestCase
{
    public function testBindAttributeWithEmptyList()
    {
        $html = '<div' . Bind::attribute('class', []) . '>flex container</div>';
        $this->assertEquals('<div>flex container</div>', $html);
    }

    public function testBindAttributeWithValueList()
    {
        $html = '<div' . Bind::attribute('class', ['flex', 'test']) . '>flex container</div>';
        $this->assertEquals('<div class="flex test">flex container</div>', $html);
    }

    public function testBindAttributeWithKeyValueListAndTrueValues()
    {
        $html = '<div' . Bind::attribute('class', ['flex' => true]) . '>flex container</div>';
        $this->assertEquals('<div class="flex">flex container</div>', $html);
    }

    public function testBindAttributeWithKeyValueListAndFalseValues()
    {
        $html = '<div' . Bind::attribute('class', ['flex' => false]) . '>flex container</div>';
        $this->assertEquals('<div>flex container</div>', $html);
    }

    public function testBindAttributeWithMixedList()
    {
        $html = '<div' . Bind::attribute('class', ['p-3', 'flex' => true, 'hidden' => false]) . '>flex container</div>';
        $this->assertEquals('<div class="p-3 flex">flex container</div>', $html);
    }

    public function testBindAttributeWithEmptyMergeList()
    {
        $merge = [];
        $html = '<div' . Bind::attribute('class', ['p-3', 'flex' => true, 'hidden' => false], $merge) . '>flex container</div>';
        $this->assertEquals('<div class="p-3 flex">flex container</div>', $html);
    }

    public function testBindAttributeWithMergeList()
    {
        $merge = ['mb-3', 'bg-primary' => false];
        $html = '<div' . Bind::attribute('class', ['p-3', 'flex' => true, 'hidden' => false], $merge) . '>flex container</div>';
        $this->assertEquals('<div class="p-3 flex mb-3">flex container</div>', $html);
    }

    public function testBindSimpleAttributeWithNullValue()
    {
        $val = null;
        $html = '<input' . Bind::attribute('value', $val) . ' />';
        $this->assertEquals('<input />', $html);

        $html = '<input' . Bind::simpleAttribute('value', $val) . ' />';
        $this->assertEquals('<input />', $html);
    }

    public function testBindSimpleAttributeWithBooleanValue()
    {
        $val = false;
        $html = '<input' . Bind::attribute('value', $val) . ' />';
        $this->assertEquals('<input />', $html);

        $html = '<input' . Bind::simpleAttribute('value', $val) . ' />';
        $this->assertEquals('<input value="" />', $html);
    }

    public function testBindSimpleAttributeWithEmptyStringValue()
    {
        $val = "";
        $html = '<input' . Bind::attribute('value', $val) . ' />';
        $this->assertEquals('<input value="" />', $html);

        $html = '<input' . Bind::simpleAttribute('value', $val) . ' />';
        $this->assertEquals('<input value="" />', $html);
    }

    public function testBindSimpleAttributeWithStringValue()
    {
        $val = "cool";
        $html = '<input' . Bind::attribute('value', $val) . ' />';
        $this->assertEquals('<input value="cool" />', $html);

        $html = '<input' . Bind::simpleAttribute('value', $val) . ' />';
        $this->assertEquals('<input value="cool" />', $html);
    }

    public function testBindSimpleAttributeWithNumericValue()
    {
        $val = 0;
        $html = '<input' . Bind::attribute('value', $val) . ' />';
        $this->assertEquals('<input value="0" />', $html);

        $html = '<input' . Bind::simpleAttribute('value', $val) . ' />';
        $this->assertEquals('<input value="0" />', $html);

        $val = 3.14;
        $html = '<input' . Bind::attribute('value', $val) . ' />';
        $this->assertEquals('<input value="3.14" />', $html);

        $html = '<input' . Bind::simpleAttribute('value', $val) . ' />';
        $this->assertEquals('<input value="3.14" />', $html);
    }

    public function testBindBooleanAttributeWithTrueValue()
    {
        $selectedColor = 'blue';
        $color = 'blue';
        $isColorBlue = $selectedColor === $color;
        $html = '<option' . Bind::attribute('selected', $isColorBlue) . '>blue</option>';
        $this->assertEquals('<option selected>blue</option>', $html);

        $html = '<option' . Bind::booleanAttribute('selected', $isColorBlue) . '>blue</option>';
        $this->assertEquals('<option selected>blue</option>', $html);
    }

    public function testBindBooleanAttributeWithFalseValue()
    {
        $selectedColor = 'red';
        $color = 'blue';
        $isColorBlue = $selectedColor === $color;
        $html = '<option' . Bind::attribute('selected', $isColorBlue) . '>blue</option>';
        $this->assertEquals('<option>blue</option>', $html);

        $html = '<option' . Bind::booleanAttribute('selected', $isColorBlue) . '>blue</option>';
        $this->assertEquals('<option>blue</option>', $html);
    }

    public function testBindDataAttributesWithEmptyList()
    {
        $html = '<div' . Bind::data([]) . '></div>';
        $this->assertEquals('<div></div>', $html);
    }

    public function testBindDataAttributesWithListAndNullValue()
    {
        $html = '<div' . Bind::data(['test' => null]) . '></div>';
        $this->assertEquals('<div data-test=""></div>', $html);
    }

    public function testBindDataAttributesWithListAndBoolValues()
    {
        $html = '<div' . Bind::data(['test' => true, 'test-second' => false]) . '></div>';
        $this->assertEquals('<div data-test="1" data-test-second=""></div>', $html);
    }

    public function testBindDataAttributesWithListAndEmptyValues()
    {
        $html = '<div' . Bind::data(['test' => '']) . '></div>';
        $this->assertEquals('<div data-test=""></div>', $html);
    }

    public function testBindDataAttributesWithListAndValues()
    {
        $html = '<div' . Bind::data(['test' => 'foo', 'test-second' => 'bar']) . '></div>';
        $this->assertEquals('<div data-test="foo" data-test-second="bar"></div>', $html);
    }

    public function testBindCssClasses()
    {
        $html = '<div' . Bind::class(['flex']) . '>flex container</div>';
        $this->assertEquals('<div class="flex">flex container</div>', $html);
    }

    public function testBindStyle()
    {
        $styles = ['border: 1px solid black', 'display: flex' => false];
        $html = '<div' . Bind::style(['color: blue', 'font-size: 32px' => true], $styles) . '>flex container</div>';
        $this->assertEquals('<div style="color: blue; font-size: 32px; border: 1px solid black;">flex container</div>', $html);
    }

    public function testBindId()
    {
        $html = '<form' . Bind::id('loginForm') . '></form>';
        $this->assertEquals('<form id="loginForm"></form>', $html);

        $html = '<form' . Bind::id(1234) . '></form>';
        $this->assertEquals('<form id="1234"></form>', $html);

        $html = '<form' . Bind::id(null) . '></form>';
        $this->assertEquals('<form></form>', $html);
    }
}
