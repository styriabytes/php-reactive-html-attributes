# Styriabytes - Reactive HTML Attributes

The reactive html attributes library provides a clean and modern way to bind reactivity to a html tag attribute.

## Installation

Reactive HTML Attributes uses [Composer](https://getcomposer.org) to install and update:

```
composer require styriabytes/reactive-html-attributes
```

## Usage

### Quick Start

The following piece of code...
```php
<?php
use Styriabytes\ReactiveHtmlAttributes\Bind;

$variant = 'success';
$cssClasses = [
    'alert',
    'alert-success' => $variant === 'success',
    'alert-warning' => $variant === 'warning',
    'alert-danger' => $variant === 'danger',
    'alert-info' => $variant === 'info',
];
?>
<div<?= Bind::class($cssClasses) ?> role="alert">
    A simple primary alert—check it out!
</div>
```
...will render:
```html
<div class="alert alert-success" role="alert">
    A simple primary alert—check it out!
</div>
```

## Contribution

Any type of feedback, pull request or issue is welcome.

We use the [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) to check for Coding Standard Issues.

### Check Coding Standard

```bash
composer checkcs

# or

vendor/bin/phpcs --standard=PSR1,PSR2,PSR12 --colors src
```

### Fix Errors Automatically

```bash
composer fixcs 

# or

vendor/bin/phpcbf --standard=PSR1,PSR2,PSR12 src
```

### Run Tests

```bash
composer test

# or

vendor/bin/phpunit
```

## License

The Styriabytes Reactive HTML Attributes library is licensed under the MIT license.
