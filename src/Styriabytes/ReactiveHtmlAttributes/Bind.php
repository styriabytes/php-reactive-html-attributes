<?php

namespace Styriabytes\ReactiveHtmlAttributes;

/**
 * @method static string class(array $cssClasses, array $mergeCssClasses = [])
 * @method static string id(?string $value)
 * @method static string name(?string $value)
 * @method static string value(?string $value)
 * @method static string placeholder(?string $value)
 * @method static string tabindex(?string $value)
 * @method static string title(?string $value)
 * @method static string disabled(bool $value)
 * @method static string selected(bool $value)
 * @method static string checked(bool $value)
 * @method static string autofocus(bool $value)
 * @method static string readonly(bool $value)
 */
class Bind
{
    /** @var array */
    protected static $attributeMapping = [
        'class' => 'attribute',
        'style' => 'attribute',
        'id' => 'simpleAttribute',
        'name' => 'simpleAttribute',
        'value' => 'simpleAttribute',
        'placeholder' => 'simpleAttribute',
        'tabindex' => 'simpleAttribute',
        'title' => 'simpleAttribute',
        'disabled' => 'booleanAttribute',
        'selected' => 'booleanAttribute',
        'checked' => 'booleanAttribute',
        'autofocus' => 'booleanAttribute',
        'readonly' => 'booleanAttribute',
    ];

    /**
     * @param string                 $name
     * @param array|bool|string|null $values
     * @param array                  $merge
     *
     * @return string
     */
    public static function attribute(string $name, $values = null, array $merge = [], string $glue = ' '): string
    {
        if ($values === null || $values === false) {
            return '';
        }

        if ($values === true) {
            return ' ' . $name;
        }

        if (!is_array($values)) {
            return " {$name}=\"{$values}\"";
        }

        if ($merge) {
            $values = array_merge($values, $merge);
        }

        $output = '';
        foreach ($values as $val => $enabled) {
            if (is_numeric($val)) {
                $output .= $enabled . $glue;
                continue;
            }
            if ($enabled) {
                $output .= $val . $glue;
            }
        }
        if (!$output) {
            return '';
        }
        return ' ' . $name . '="' . substr($output, 0, -1) . '"';
    }

    /**
     * @param string      $name
     * @param string|null $text
     *
     * @return string
     */
    public static function simpleAttribute(string $name, ?string $text): string
    {
        return self::attribute($name, $text);
    }

    /**
     * @param string $name
     * @param bool   $isTrue
     *
     * @return string
     */
    public static function booleanAttribute(string $name, bool $isTrue = false): string
    {
        return self::attribute($name, $isTrue);
    }

    /**
     * @param array $list
     * @param array $merge
     *
     * @return string
     */
    public static function data(array $list, array $merge = []): string
    {
        if (!$list) {
            return '';
        }
        $output = '';
        foreach ($list as $key => $val) {
            $output .= " data-{$key}=\"{$val}\"";
        }
        return $output;
    }

    /**
     * @param array $styles
     * @param array $mergeStyles
     *
     * @return string
     */
    public static function style(array $styles, array $mergeStyles = [])
    {
        return self::attribute('style', $styles, $mergeStyles, '; ');
    }

    /**
     * @param string $name
     * @param array $arguments
     *
     * @return void
     */
    public static function __callStatic(string $name, array $arguments)
    {
        if (!isset(self::$attributeMapping[$name])) {
            throw new \RuntimeException("The method '{$name}' is not supported.");
        }
        return call_user_func([__CLASS__, self::$attributeMapping[$name]], $name, ...$arguments);
    }
}
